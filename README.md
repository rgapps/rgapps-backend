# Useful commands

### Run database
```
docker-compose -f cicd/docker-compose.yml --project-directory . up -d database
```

### Run locally
- Run the database.
- Run the Application main class.
#### swagger
After running locally swagger documentation is in http://localhost:8080/swagger-ui.html

### build docker
```
docker build -f cicd/build/Dockerfile .
```

### build with docker compose
```
docker-compose -f cicd/docker-compose.yml --project-directory . build rgapps-backend
```

### run with docker compose
``` 
docker-compose -f cicd/docker-compose.yml --project-directory . up --build -d
``` 

> *NOTE:* run  these commands from the project directory
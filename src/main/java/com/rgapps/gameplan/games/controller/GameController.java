package com.rgapps.gameplan.games.controller;

import com.rgapps.gameplan.games.controller.resource.GameResource;
import com.rgapps.gameplan.games.model.Game;
import com.rgapps.gameplan.games.service.GameService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/games")
@RequiredArgsConstructor
public class GameController {

    @NonNull private final GameService service;

    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<List<Game>> games() {
        return service.getGames();
    }

    @GetMapping("/{id}")
    public Mono<Game> getGame(@PathVariable("id") String id) {
        return service.getGame(UUID.fromString(id));
    }

    @PostMapping
    public Mono<Game> addGame(@RequestBody GameResource resource) {
        return service.addGame(resource);
    }

    @DeleteMapping("/{id}")
    public Mono<Game> deleteGame(@PathVariable("id") String id) {
        return service.deleteGame(UUID.fromString(id));
    }

    @PutMapping("/{id}")
    public Mono<Game> updateGame(@PathVariable("id") String id, @RequestBody GameResource resource) {
        return service.updateGame(UUID.fromString(id), resource);
    }
}

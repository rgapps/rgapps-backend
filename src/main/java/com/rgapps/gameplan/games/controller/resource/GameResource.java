package com.rgapps.gameplan.games.controller.resource;

import com.rgapps.gameplan.games.model.Game;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GameResource {
    private String title;
    private String imageUrl;

    public Game toGame() {
        return new Game(title, imageUrl);
    }
}

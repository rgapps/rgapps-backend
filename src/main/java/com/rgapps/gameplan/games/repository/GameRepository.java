package com.rgapps.gameplan.games.repository;

import com.rgapps.gameplan.games.model.Game;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface GameRepository extends ReactiveMongoRepository<Game, UUID> {
    Flux<Game> findAllByStatus(Game.Status status);

    Mono<Game> findByIdAndStatus(UUID id, Game.Status status);
}

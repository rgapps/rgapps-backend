package com.rgapps.gameplan.games.service;

import com.rgapps.gameplan.games.cache.GamesCache;
import com.rgapps.gameplan.games.controller.resource.GameResource;
import com.rgapps.gameplan.games.model.Game;
import com.rgapps.gameplan.games.repository.GameRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GameServiceImpl implements GameService {
    @NonNull
    private final GameRepository gameRepository;
    @NonNull
    private final GamesCache gamesCache;

    public Flux<List<Game>> getGames() {
        return gamesCache.getAllStream();
    }

    public Mono<Game> getGame(UUID id) {
        return gameRepository.findByIdAndStatus(id, Game.Status.ACTIVE);
    }

    public Mono<Game> addGame(GameResource resource) {
        return gameRepository.save(resource.toGame())
                .flatMap(this::onCreated);
    }

    public Mono<Game> updateGame(UUID id, GameResource resource) {
        return gameRepository.findByIdAndStatus(id, Game.Status.ACTIVE)
                .flatMap(it -> gameRepository.save(it.updateGame(resource)))
                .flatMap(this::onUpdated);
    }

    public Mono<Game> deleteGame(UUID id) {
        return gameRepository.findByIdAndStatus(id, Game.Status.ACTIVE)
                .flatMap(it -> gameRepository.save(it.updateStatus(Game.Status.DELETED)))
                .flatMap(this::onDeleted);
    }

    private Mono<Game> onDeleted(Game game) {
        return Mono.just(gamesCache.delete(game));
    }

    private Mono<Game> onUpdated(Game game) {
        return Mono.just(gamesCache.update(game));
    }

    private Mono<Game> onCreated(Game game) {
        return Mono.just(gamesCache.update(game));
    }
}

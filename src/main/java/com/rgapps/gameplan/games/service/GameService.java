package com.rgapps.gameplan.games.service;

import com.rgapps.gameplan.games.controller.resource.GameResource;
import com.rgapps.gameplan.games.model.Game;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

public interface GameService {
    Flux<List<Game>> getGames();
    Mono<Game> getGame(UUID id);
    Mono<Game> addGame(GameResource resource);
    Mono<Game> updateGame(UUID id, GameResource resource);
    Mono<Game> deleteGame(UUID id);
}

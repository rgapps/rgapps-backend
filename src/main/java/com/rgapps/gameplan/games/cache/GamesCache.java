package com.rgapps.gameplan.games.cache;

import com.rgapps.gameplan.games.model.Game;
import com.rgapps.gameplan.games.repository.GameRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class GamesCache {
    private final Sinks.EmitFailureHandler emissionFailureHandler = (signalType, emission) -> emission.equals(Sinks.EmitResult.FAIL_NON_SERIALIZED);
    private final Sinks.Many<List<Game>> cacheCollection = Sinks.many().replay().latest();
    private final Sinks.Many<Game> cache = Sinks.many().multicast().onBackpressureBuffer();

    private final AtomicBoolean initialized = new AtomicBoolean(false);
    @NonNull private final GameRepository gameRepository;

    public Flux<Game> getStream() {
        return cache.asFlux();
    }

    public Flux<List<Game>> getAllStream() {
        return Mono.just(initialized)
                .flatMapMany(it -> {
                    if (!initialized.get()) {
                        initialized.set(true);
                        return gameRepository.findAllByStatus(Game.Status.ACTIVE).collectList()
                                .doOnNext(games -> cacheCollection.emitNext(games, emissionFailureHandler))
                                .thenMany(cacheCollection.asFlux());
                    }
                    return cacheCollection.asFlux();
                });
    }

    public Game update(Game game) {
        cache.emitNext(game, emissionFailureHandler);
        cacheCollection.asFlux().take(1).subscribe(list -> {
            List<Game> newList = list.stream().filter(g -> !g.getId().equals(game.getId())).collect(Collectors.toList());
            newList.add(game);
            cacheCollection.emitNext(newList, emissionFailureHandler);
        });
        return game;
    }

    public Game delete(Game game) {
        cache.emitNext(game, emissionFailureHandler);
        cacheCollection.asFlux().take(1).subscribe(list -> {
            List<Game> newList = list.stream().filter(g -> !g.getId().equals(game.getId())).collect(Collectors.toList());
            cacheCollection.emitNext(newList, emissionFailureHandler);
        });
        return game;
    }
}

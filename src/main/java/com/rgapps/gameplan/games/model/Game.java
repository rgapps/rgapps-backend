package com.rgapps.gameplan.games.model;

import com.rgapps.gameplan.games.controller.resource.GameResource;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Document("game")
@RequiredArgsConstructor
public class Game {
    public enum Status {
        DELETED, ACTIVE
    }

    @Id private UUID id = UUID.randomUUID();
    @NonNull private String title;
    @NonNull private String imageUrl;
    private LocalDateTime createdDate = LocalDateTime.now();
    private LocalDateTime updatedDate = createdDate;

    private Status status = Status.ACTIVE;

    public Game updateStatus(Status status) {
        this.status = status;
        this.updatedDate = LocalDateTime.now();
        return this;
    }

    public Game updateGame(GameResource resource) {
        this.title = resource.getTitle();
        this.imageUrl = resource.getImageUrl();
        this.updatedDate = LocalDateTime.now();
        return this;
    }
}

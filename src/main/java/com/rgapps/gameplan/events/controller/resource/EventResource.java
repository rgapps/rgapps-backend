package com.rgapps.gameplan.events.controller.resource;

import com.rgapps.gameplan.events.model.Event;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class EventResource {
    private String location;
    private Double cost;
    private LocalDateTime date;
    private Long duration;
    private String meetupLink;

    public Event toEvent() {
        return new Event(location, cost, date, duration, meetupLink);
    }
}

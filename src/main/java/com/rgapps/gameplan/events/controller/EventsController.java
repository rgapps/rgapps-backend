package com.rgapps.gameplan.events.controller;

import com.rgapps.gameplan.events.controller.resource.EventResource;
import com.rgapps.gameplan.events.model.Event;
import com.rgapps.gameplan.events.service.EventService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/events")
@RequiredArgsConstructor
public class EventsController {

    @NonNull private final EventService service;

    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<List<Event>> games() {
        return service.getEvents();
    }

    @GetMapping("/{id}")
    public Mono<Event> getEvent(@PathVariable("id") String id) {
        return service.getEvent(UUID.fromString(id));
    }

    @PostMapping
    public Mono<Event> addEvent(@RequestBody EventResource resource) {
        return service.addEvent(resource);
    }

    @DeleteMapping("/{id}")
    public Mono<Event> deleteEvent(@PathVariable("id") String id) {
        return service.deleteEvent(UUID.fromString(id));
    }

    @PutMapping("/{id}")
    public Mono<Event> updateEvent(@PathVariable("id") String id, @RequestBody EventResource resource) {
        return service.updateEvent(UUID.fromString(id), resource);
    }
}

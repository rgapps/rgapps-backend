package com.rgapps.gameplan.events.service;

import com.rgapps.gameplan.events.controller.resource.EventResource;
import com.rgapps.gameplan.events.model.Event;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

public interface EventService {
    Flux<List<Event>> getEvents();
    Mono<Event> getEvent(UUID id);
    Mono<Event> addEvent(EventResource resource);
    Mono<Event> updateEvent(UUID id, EventResource resource);
    Mono<Event> deleteEvent(UUID id);
}

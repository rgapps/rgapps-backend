package com.rgapps.gameplan.events.service;

import com.rgapps.gameplan.events.cache.EventsCache;
import com.rgapps.gameplan.events.controller.resource.EventResource;
import com.rgapps.gameplan.events.model.Event;
import com.rgapps.gameplan.events.repository.EventRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {
    @NonNull
    private final EventRepository eventRepository;
    @NonNull
    private final EventsCache eventsCache;

    public Flux<List<Event>> getEvents() {
        return eventsCache.getAllStream();
    }

    public Mono<Event> getEvent(UUID id) {
        return eventRepository.findByIdAndStatus(id, Event.Status.ACTIVE);
    }

    public Mono<Event> addEvent(EventResource resource) {
        return eventRepository.save(resource.toEvent())
                .flatMap(this::onCreated);
    }

    public Mono<Event> updateEvent(UUID id, EventResource resource) {
        return eventRepository.findByIdAndStatus(id, Event.Status.ACTIVE)
                .flatMap(it -> eventRepository.save(it.updateEvent(resource)))
                .flatMap(this::onUpdated);
    }

    public Mono<Event> deleteEvent(UUID id) {
        return eventRepository.findByIdAndStatus(id, Event.Status.ACTIVE)
                .flatMap(it -> eventRepository.save(it.updateStatus(Event.Status.DELETED)))
                .flatMap(this::onDeleted);
    }

    private Mono<Event> onDeleted(Event event) {
        return Mono.just(eventsCache.delete(event));
    }

    private Mono<Event> onUpdated(Event event) {
        return Mono.just(eventsCache.update(event));
    }

    private Mono<Event> onCreated(Event event) {
        return Mono.just(eventsCache.update(event));
    }
}

package com.rgapps.gameplan.events.model;

import com.rgapps.gameplan.events.controller.resource.EventResource;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Document
@RequiredArgsConstructor
public class Event {
    @Id protected final UUID id = UUID.randomUUID();

    public enum Status {
        DELETED, ACTIVE
    }

    @NonNull private String location;
    @NonNull private Double cost;
    @NonNull private LocalDateTime date;
    @NonNull private Long duration;
    @NonNull private String meetupLink;
    private Status status = Status.ACTIVE;


    private final LocalDateTime createdDate = LocalDateTime.now();
    private LocalDateTime updatedDate = createdDate;

    public Event updateStatus(Event.Status status) {
        this.status = status;
        this.updatedDate = LocalDateTime.now();
        return this;
    }

    public Event updateEvent(EventResource resource) {
        this.location = resource.getLocation();
        this.cost = resource.getCost();
        this.date = resource.getDate();
        this.duration = resource.getDuration();
        this.meetupLink = resource.getMeetupLink();
        this.updatedDate = LocalDateTime.now();
        return this;
    }
}

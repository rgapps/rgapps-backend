package com.rgapps.gameplan.events.cache;

import com.rgapps.gameplan.events.model.Event;
import com.rgapps.gameplan.events.repository.EventRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class EventsCache {
    private final Sinks.EmitFailureHandler emissionFailureHandler = (signalType, emission) -> emission.equals(Sinks.EmitResult.FAIL_NON_SERIALIZED);
    private final Sinks.Many<List<Event>> cacheCollection = Sinks.many().replay().latest();
    private final Sinks.Many<Event> cache = Sinks.many().multicast().onBackpressureBuffer();

    private final AtomicBoolean initialized = new AtomicBoolean(false);
    @NonNull private final EventRepository eventRepository;

    public Flux<Event> getStream() {
        return cache.asFlux();
    }

    public Flux<List<Event>> getAllStream() {
        return Mono.just(initialized)
                .flatMapMany(it -> {
                    if (!initialized.get()) {
                        initialized.set(true);
                        return eventRepository.findAllByStatusOrderByCreatedDateDesc(Event.Status.ACTIVE).collectList()
                                .doOnNext(events -> cacheCollection.emitNext(events, emissionFailureHandler))
                                .thenMany(cacheCollection.asFlux());
                    }
                    return cacheCollection.asFlux();
                });
    }

    public Event update(Event event) {
        cache.emitNext(event, emissionFailureHandler);
        cacheCollection.asFlux().take(1).subscribe(list -> {
            List<Event> newList = list.stream().filter(g -> !g.getId().equals(event.getId())).collect(Collectors.toList());
            newList.add(event);
            cacheCollection.emitNext(newList, emissionFailureHandler);
        });
        return event;
    }

    public Event delete(Event event) {
        cache.emitNext(event, emissionFailureHandler);
        cacheCollection.asFlux().take(1).subscribe(list -> {
            List<Event> newList = list.stream().filter(g -> !g.getId().equals(event.getId())).collect(Collectors.toList());
            cacheCollection.emitNext(newList, emissionFailureHandler);
        });
        return event;
    }
}

package com.rgapps.gameplan.events.repository;

import com.rgapps.gameplan.events.model.Event;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface EventRepository extends ReactiveMongoRepository<Event, UUID> {
    Flux<Event> findAllByStatusOrderByCreatedDateDesc(Event.Status status);

    Mono<Event> findByIdAndStatus(UUID id, Event.Status status);
}

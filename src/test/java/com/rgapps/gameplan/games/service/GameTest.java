package com.rgapps.gameplan.games.service;

import com.rgapps.gameplan.games.cache.GamesCache;
import com.rgapps.gameplan.games.controller.resource.GameResource;
import com.rgapps.gameplan.games.model.Game;
import com.rgapps.gameplan.games.repository.GameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {GamesCache.class, GameServiceImpl.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class GameTest {

    @Autowired
    private GameServiceImpl gameService;
    @MockBean
    private GameRepository repository;

    @BeforeEach
    void clean() {
        Mockito.reset(repository);
    }

    @Test
    public void shouldCreateCorrectlyAGame() {
        GameResource resource = GameResource.builder()
                .title("title")
                .imageUrl("image.url")
                .build();

        Mockito.when(repository.save(any(Game.class))).thenAnswer(invocation -> Mono.just(invocation.getArguments()[0]));

        Mono<Game> createdGame = gameService.addGame(resource);

        StepVerifier.create(createdGame)
                .expectNextMatches(resp ->
                        resp.getTitle().equals(resource.getTitle()) &&
                                resp.getImageUrl().equals(resource.getImageUrl()) &&
                                resp.getStatus().equals(Game.Status.ACTIVE)
                )
                .verifyComplete();

        Mockito.verify(repository, Mockito.times(1)).save(any(Game.class));
        Mockito.verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldGetCorrectlyAGame() {
        Game game = GameResource.builder()
                .title("title")
                .imageUrl("image.url")
                .build().toGame();

        Mockito.when(repository.findByIdAndStatus(game.getId(), Game.Status.ACTIVE)).thenReturn(Mono.just(game));

        Mono<Game> retrievedGame = gameService.getGame(game.getId());

        StepVerifier.create(retrievedGame)
                .expectNextMatches(resp ->
                        resp.getId().equals(game.getId()) &&
                                resp.getTitle().equals(game.getTitle()) &&
                                resp.getImageUrl().equals(game.getImageUrl()) &&
                                resp.getStatus().equals(Game.Status.ACTIVE)
                )
                .verifyComplete();

        Mockito.verify(repository, Mockito.times(1)).findByIdAndStatus(game.getId(), Game.Status.ACTIVE);
        Mockito.verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldDeleteCorrectlyAGame() {
        Game game = GameResource.builder()
                .title("title")
                .imageUrl("image.url")
                .build().toGame();

        Mockito.when(repository.findByIdAndStatus(any(), eq(Game.Status.ACTIVE))).thenReturn(Mono.just(game));
        Mockito.when(repository.save(any(Game.class))).thenAnswer(invocation -> Mono.just(invocation.getArguments()[0]));

        Mono<Game> deletedGame = gameService.deleteGame(game.getId());

        StepVerifier.create(deletedGame)
                .expectNextMatches(resp ->
                        resp.getId().equals(game.getId()) &&
                                resp.getTitle().equals(game.getTitle()) &&
                                resp.getImageUrl().equals(game.getImageUrl()) &&
                                resp.getStatus().equals(Game.Status.DELETED)
                )
                .verifyComplete();

        Mockito.verify(repository, Mockito.times(1)).findByIdAndStatus(game.getId(), Game.Status.ACTIVE);
        Mockito.verify(repository, Mockito.times(1)).save(any(Game.class));
        Mockito.verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldUpdateCorrectlyAGame() {
        GameResource initialResource = GameResource.builder()
                .title("title")
                .imageUrl("image.url")
                .build();
        Game game = initialResource.toGame();
        GameResource resourceUpdated = GameResource.builder()
                .title("title2")
                .imageUrl("image.url2")
                .build();

        Mockito.when(repository.findByIdAndStatus(eq(game.getId()), eq(Game.Status.ACTIVE))).thenReturn(Mono.just(game));
        Mockito.when(repository.save(any(Game.class))).thenAnswer(invocation -> Mono.just(invocation.getArguments()[0]));

        Mono<Game> updatedGame = gameService.updateGame(game.getId(), resourceUpdated);

        StepVerifier.create(updatedGame)
                .expectNextMatches(resp ->
                        resp.getId().equals(game.getId()) &&
                                resp.getTitle().equals(resourceUpdated.getTitle()) &&
                                resp.getImageUrl().equals(resourceUpdated.getImageUrl()) &&
                                resp.getStatus().equals(Game.Status.ACTIVE)
                )
                .verifyComplete();

        Mockito.verify(repository, Mockito.times(1)).findByIdAndStatus(game.getId(), Game.Status.ACTIVE);
        Mockito.verify(repository, Mockito.times(1)).save(any(Game.class));
        Mockito.verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldGetGamesCorrectly() {
        Game game1 = GameResource.builder()
                .title("title")
                .imageUrl("image.url")
                .build().toGame();
        Game game2 = GameResource.builder()
                .title("title2")
                .imageUrl("image.url2")
                .build().toGame();

        Mockito.when(repository.findAllByStatus(eq(Game.Status.ACTIVE))).thenReturn(Flux.just(game1, game2));

        Flux<List<Game>> createdGames = gameService.getGames();

        StepVerifier.create(createdGames)
                .consumeNextWith(resp -> {
                            assertThat(resp.size()).isEqualTo(2);
                            assertThat(resp).contains(game1, game2);
                        }
                )
                .expectNoEvent(Duration.ZERO)
                .thenCancel()
                .verify();

        Mockito.verify(repository, Mockito.times(1)).findAllByStatus(Game.Status.ACTIVE);
        Mockito.verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldRetrieveGamesStreamForMultipleSubscribers() {
        Game game1 = GameResource.builder()
                .title("title")
                .imageUrl("image.url")
                .build().toGame();
        Game game2 = GameResource.builder()
                .title("title2")
                .imageUrl("image.url2")
                .build().toGame();

        Mockito.when(repository.findAllByStatus(eq(Game.Status.ACTIVE))).thenReturn(Flux.just(game1));

        Flux<List<Game>> games1 = gameService.getGames();
        Flux<List<Game>> games2 = gameService.getGames();
        Flux<List<Game>> games3 = gameService.getGames();

        StepVerifier.create(games1)
                .consumeNextWith(resp -> {
                            assertThat(resp.size()).isEqualTo(1);
                            assertThat(resp).contains(game1);
                        }
                )
                .expectNoEvent(Duration.ZERO)
                .thenCancel()
                .verify();

        StepVerifier.create(games2)
                .consumeNextWith(resp -> {
                            assertThat(resp.size()).isEqualTo(1);
                            assertThat(resp).contains(game1);
                        }
                )
                .expectNoEvent(Duration.ZERO)
                .thenCancel()
                .verify();

        StepVerifier.create(games3)
                .consumeNextWith(resp -> {
                            assertThat(resp.size()).isEqualTo(1);
                            assertThat(resp).contains(game1);
                        }
                )
                .expectNoEvent(Duration.ZERO)
                .thenCancel()
                .verify();

        Mockito.verify(repository, Mockito.times(1)).findAllByStatus(Game.Status.ACTIVE);
        Mockito.verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldRetrieveGamesStreamDuringMutations() {
        Game game1 = GameResource.builder()
                .title("title")
                .imageUrl("image.url")
                .build().toGame();
        GameResource newResource = GameResource.builder()
                .title("title2")
                .imageUrl("image.url2")
                .build();
        Game game2 = newResource.toGame();

        Mockito.when(repository.findByIdAndStatus(eq(game1.getId()), eq(Game.Status.ACTIVE))).thenReturn(Mono.just(game1));
        Mockito.when(repository.findAllByStatus(eq(Game.Status.ACTIVE))).thenReturn(Flux.just(game1));
        Mockito.when(repository.save(any(Game.class))).thenAnswer(invocation -> Mono.just(invocation.getArguments()[0]));

        Flux<List<Game>> games1 = gameService.getGames();

        StepVerifier.create(games1)
                .consumeNextWith(resp -> {
                            assertThat(resp.size()).isEqualTo(1);
                            assertThat(resp).contains(game1);
                        }
                )
                .expectNoEvent(Duration.ZERO)
                .then(() -> gameService.addGame(newResource).subscribe())
                .consumeNextWith(resp -> {
                            assertThat(resp.size()).isEqualTo(2);
                            assertThat(resp).map(Game::getTitle).contains(game1.getTitle(), game2.getTitle());
                        }
                )
                .expectNoEvent(Duration.ZERO)
                .then(() -> gameService.deleteGame(game1.getId()).subscribe())
                .consumeNextWith(resp -> {
                            assertThat(resp.size()).isEqualTo(1);
                            assertThat(resp).map(Game::getTitle).contains(game2.getTitle());
                        }
                )
                .expectNoEvent(Duration.ZERO)
                .thenCancel()
                .verify();

        Mockito.verify(repository, Mockito.times(1)).findAllByStatus(Game.Status.ACTIVE);
        Mockito.verify(repository, Mockito.times(1)).findByIdAndStatus(game1.getId(), Game.Status.ACTIVE);
        Mockito.verify(repository, Mockito.times(2)).save(any(Game.class));
        Mockito.verifyNoMoreInteractions(repository);
    }
}
